import * as Router from 'koa-router';
import { Context } from 'koa';
import { Model, Page } from 'objection';

import educationsRouter from './educations';
import homeRouter from './home';
import usersRouter from './users';
import worksRouter from './works';
import Work from '../models/work';

const router = new Router();

router.use('/', homeRouter.routes());
router.use('/users', usersRouter.routes());

router.use('/users/:orcidId/educations', educationsRouter.routes());
router.use('/users/:orcidId/works', worksRouter.routes());

type MustReturn = Model | Model[] | Page<Model> | JSON | string;

export async function returnData(ctx: Context, promise: Promise<MustReturn>): Promise<void> {
  await promise
    .then(data => {
      ctx.body = data;
      ctx.status = 200;
    })
    .catch(err => {
      console.error(err);

      try {
        ctx.body = { error: JSON.parse(err.message) };
      } catch (e) {
        ctx.body = { error: err.message };
      }

      ctx.status = 400;
    });
}

export default router;
