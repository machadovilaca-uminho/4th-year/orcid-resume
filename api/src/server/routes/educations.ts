import * as Router from 'koa-router';
import { returnData } from './index';
import educationsController from '../controllers/educations';

const router = new Router();

router.get('/', async ctx => {
  const page = ctx.query.page || 0;

  await returnData(ctx, educationsController.get(ctx.params.orcidId, page));
});

router.get('/update', async ctx => {
  await returnData(ctx, educationsController.create(ctx.params.orcidId));
});

export default router;
