import * as Router from 'koa-router';
import { returnData } from './index';
import usersController from '../controllers/users';

const router = new Router();

router.get('/:orcidId', async ctx => {
  await returnData(ctx, usersController.get(ctx.params.orcidId));
});

router.get('/:orcidId/update', async ctx => {
  await returnData(ctx, usersController.create(ctx.params.orcidId));
});

export default router;
