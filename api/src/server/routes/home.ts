import * as Router from 'koa-router';
import { createReadStream } from 'fs';

const router = new Router();

router.get('/', ctx => {
  ctx.type = 'html';
  ctx.body = createReadStream(`${__dirname}/../static/page.html`);
});

export default router;
