import * as Router from 'koa-router';
import { returnData } from './index';
import worksController from '../controllers/works';

const router = new Router();

router.get('/', async ctx => {
  const page = ctx.query.page || 0;

  await returnData(ctx, worksController.get(ctx.params.orcidId, page));
});

router.get('/:id', async ctx => {
  await returnData(ctx, worksController.show(ctx.params.orcidId, ctx.params.id));
});

router.get('/update', async ctx => {
  await returnData(ctx, worksController.create(ctx.params.orcidId));
});

export default router;
