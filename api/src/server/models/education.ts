import { Model, RelationMappings } from 'objection';
import { OrcidEducation } from '../middlewares/orcid/educations';
import { orcidDateToDate } from '../middlewares/orcid/date';
import Organization from './organization';
import User from './user';

class Education extends Model {

  id: string;
  user: User;
  orcidId: string;
  lastUpdateDate: Date;
  departmentName: string;
  roleTitle: string;
  startDate: Date;
  endDate: Date;
  organization: Organization;

  static get tableName(): string {
    return 'educations';
  }

  static get relationMappings(): RelationMappings {
    return {
      organization: {
        join: {
          from: 'educations.organizationId',
          to: 'organizations.id'
        },
        modelClass: Organization,
        relation: Model.BelongsToOneRelation
      },
      user: {
        join: {
          from: 'educations.userId',
          to: 'users.id'
        },
        modelClass: User,
        relation: Model.BelongsToOneRelation
      }
    };
  }

}

export function educationFromOrcid(orcidId: string, orcidEducation: OrcidEducation): Education {
  const education = new Education();

  education.orcidId = orcidEducation['put-code'];
  education.lastUpdateDate = new Date(orcidEducation['last-modified-date'].value);
  education.departmentName = orcidEducation['department-name'];
  education.roleTitle = orcidEducation['role-title'];
  education.startDate = orcidDateToDate(orcidEducation['start-date']);
  education.endDate = orcidDateToDate(orcidEducation['end-date']);

  return education;
}

export default Education;
