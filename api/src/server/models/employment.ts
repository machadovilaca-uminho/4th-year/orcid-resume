import { Model, RelationMappings } from 'objection';
import Organization from './organization';
import User from './user';

class Employment extends Model {

  id: string;
  user: User;
  orcidId: string;
  lastUpdateDate: Date;
  departmentName: string;
  roleTitle: string;
  startDate: string;
  endDate: string;
  url: string;
  organization: Organization;

  static get tableName(): string {
    return 'employments';
  }

  static get relationMappings(): RelationMappings {
    return {
      organization: {
        join: {
          from: 'employments.organizationId',
          to: 'organizations.id'
        },
        modelClass: Organization,
        relation: Model.BelongsToOneRelation
      }
    };
  }

}

export default Employment;
