import { Model, RelationMappings } from 'objection';
import { OrcidOrganization } from '../middlewares/orcid/organization';
import Address, { createOrGetAddress } from './address';

class Organization extends Model {

  id: string;
  orcidId: string;
  name: string;
  address: Address;

  static get tableName(): string {
    return 'organizations';
  }

  static get relationMappings(): RelationMappings {
    return {
      address: {
        join: {
          from: 'organizations.addressId',
          to: 'addresses.id'
        },
        modelClass: Address,
        relation: Model.BelongsToOneRelation
      }
    };
  }

}

function organizationFromOrcidOrganization(orcidId: string, orcidOrganization: OrcidOrganization): Organization {
  const organization = new Organization();

  organization.orcidId = orcidId;
  organization.name = orcidOrganization.name;

  return organization;
}

export async function createOrGetOrganization(orcidOrganization: OrcidOrganization): Promise<Organization> {
  let orcidId = null;
  let organization: Organization = null;

  if (orcidOrganization['disambiguated-organization'] !== null) {
    orcidId = orcidOrganization['disambiguated-organization']['disambiguated-organization-identifier'];

    organization = await Organization.query()
      .select('*')
      .where('orcidId', '=', orcidId)
      .first();
  }

  if (!organization) {
    const newOrg = await organizationFromOrcidOrganization(orcidId, orcidOrganization);

    newOrg.$setRelated('address', await createOrGetAddress(orcidOrganization.address));

    return Organization.query().insertGraph(newOrg, { relate: true });
  }

  return new Promise(resolve => resolve(organization));
}

export default Organization;
