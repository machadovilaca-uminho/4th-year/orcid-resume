import { Model, RelationMappings } from 'objection';
import { OrcidExternalId, OrcidPerson } from '../middlewares/orcid/person';
import Education from './education';
import Employment from './employment';
import Work from './work';

class User extends Model {

  id: string;
  orcidId: string;
  scopusAuthorId: string;
  lastUpdateDate: Date;
  givenNames: string;
  familyName: string;
  biography: string;

  static get tableName(): string {
    return 'users';
  }

  static get relationMappings(): RelationMappings {
    return {
      educations: {
        join: {
          from: 'users.id',
          to: 'educations.userId'
        },
        modelClass: Education,
        relation: Model.HasManyRelation
      },
      employments: {
        join: {
          from: 'users.id',
          to: 'employments.userId'
        },
        modelClass: Employment,
        relation: Model.HasManyRelation
      },
      works: {
        join: {
          from: 'users.id',
          to: 'works.userId'
        },
        modelClass: Work,
        relation: Model.HasManyRelation
      }
    };
  }

}

export async function createOrUpdateUser(newUser: User): Promise<User> {
  let user: User = await User.query()
    .select('*')
    .where('orcidId', '=', newUser.orcidId)
    .first();

  if (user) {
    if (newUser.lastUpdateDate > user.lastUpdateDate) {
      User.query().findById(user.id).patch({
        biography: newUser.biography,
        familyName: newUser.familyName,
        givenNames: newUser.givenNames,
        lastUpdateDate: newUser.lastUpdateDate
      });

      user = await User.query().findById(user.id);
    }
  } else {
    user = await User.query().insert(newUser);
  }

  return new Promise(resolve => resolve(user));
}

function getScupusId(person: OrcidPerson): string {
  const eids: [OrcidExternalId] = person['external-identifiers']['external-identifier'];

  for (const eid of eids) {
    if (eid['external-id-type'] === 'Scopus Author ID') {
      return eid['external-id-value'];
    }
  }

  return null;
}

export function userFromOrcid(orcidId: string, orcidPerson: OrcidPerson): User {
  const user = new User();

  user.orcidId = orcidId;
  user.scopusAuthorId = getScupusId(orcidPerson);
  user.lastUpdateDate = orcidPerson['last-modified-date'] !== null ? new Date(orcidPerson['last-modified-date'].value) : null;
  user.givenNames = orcidPerson.name['given-names'].value;
  user.familyName = orcidPerson.name['family-name'].value;
  user.biography = orcidPerson.biography ? orcidPerson.biography.content : null;

  return user;
}

export default User;
