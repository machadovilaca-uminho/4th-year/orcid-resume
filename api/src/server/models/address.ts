import { Model, RelationMappings } from 'objection';
import { OrcidAddress } from '../middlewares/orcid/address';
import Organization from './organization';

class Address extends Model {

  id: string;
  country: string;
  region: string;
  city: string;

  static get tableName(): string {
    return 'addresses';
  }

  static get relationMappings(): RelationMappings {
    return {
      organization: {
        join: {
          from: 'addresses.id',
          to: 'organizations.addressId'
        },
        modelClass: Organization,
        relation: Model.HasManyRelation
      }
    };
  }

}

export function addressFromOrcidAddress(orcidAddress: OrcidAddress): Address {
  const address = new Address();

  address.country = orcidAddress.country;
  address.region = orcidAddress.region;
  address.city = orcidAddress.city;

  return address;
}

export async function createOrGetAddress(orcidAddress: OrcidAddress): Promise<Address> {
  const address: Address = await Address.query()
    .select('*')
    .where('country', '=', orcidAddress.country)
    .where('region', '=', orcidAddress.region)
    .where('city', '=', orcidAddress.city)
    .first();

  if (!address) {
    const newAddress = addressFromOrcidAddress(orcidAddress);

    return Address.query().insert(newAddress);
  }

  return new Promise(resolve => resolve(address));
}

export default Address;
