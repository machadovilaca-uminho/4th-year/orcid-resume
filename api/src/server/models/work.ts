import { Model, RelationMappings } from 'objection';
import { OrcidWork } from '../middlewares/orcid/works';
import { orcidDateToDate } from '../middlewares/orcid/date';
import User from './user';

class Work extends Model {

  id: string;
  user: User;
  orcidId: string;
  lastUpdateDate: Date;
  title: string;
  subtitle: string;
  type: string;
  date: Date;
  eid: string;
  doi: string;
  sourceName: string;
  citations: string;

  static get tableName(): string {
    return 'works';
  }

  static get relationMappings(): RelationMappings {
    return {
      user: {
        join: {
          from: 'works.userId',
          to: 'users.id'
        },
        modelClass: User,
        relation: Model.BelongsToOneRelation
      }
    };
  }

}

export function workFromOrcid(orcidId: string, orcidWork: OrcidWork): Work {
  const work = new Work();

  work.orcidId = orcidWork['put-code'];
  work.lastUpdateDate = new Date(orcidWork['last-modified-date'].value);
  work.title = orcidWork.title.title.value;
  work.subtitle = orcidWork.title.subtitle;
  work.type = orcidWork.type;
  work.date = orcidDateToDate(orcidWork['publication-date']);
  work.sourceName = orcidWork.source['source-name'].value;

  if (orcidWork['external-ids'] !== null) {
    orcidWork['external-ids']['external-id'].forEach(async ext => {
      if (ext['external-id-type'] === 'eid') {
        work.eid = ext['external-id-value'];
      } else if (ext['external-id-type'] === 'doi') {
        work.doi = ext['external-id-value'];
      }
    });
  }

  return work;
}

export default Work;
