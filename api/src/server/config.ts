/* eslint no-process-env: 0 */

const config = {
  elsevierApiKey: process.env.ELSEVIER_API_KEY || 'aaa111',
  environment: process.env.NODE_ENV || 'development',
  knex: {
    client: 'pg',
    connection: {
      database: process.env.DATABASE_SERVICE_DATABASE || 'orcid-resume',
      host: process.env.DATABASE_SERVICE_HOST || 'localhost',
      password: process.env.DATABASE_SERVICE_PASSWORD || '123456',
      port: process.env.DATABASE_SERVICE_PORT || 5432,
      user: process.env.DATABASE_SERVICE_USER || 'postgres'
    },
    migrations: {
      directory: './migrations/',
      tableName: process.env.DATABASE_SERVICE_DATABASE || 'orcid-resume'
    },
    pool: {
      max: 10,
      min: 2
    }
  },
  port: process.env.NODE_PORT || 3000,
  prettyLog: process.env.NODE_ENV === 'development'
};

export { config };
