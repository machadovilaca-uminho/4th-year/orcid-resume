import { config } from '../../config';
import got from 'got';

const baseUrl = 'https://api.elsevier.com/';

export interface Citations {
  'search-results': {
    'entry': [
      {
        'citedby-count': string;
      }
    ];
  };
}

export function getCitations(doi: string): Promise<Citations> {
  return got(
    `${baseUrl}/content/search/scopus?query=${doi}&field=citedby-count&apiKey=${config.elsevierApiKey}`
  ).json() as Promise<Citations>;
}
