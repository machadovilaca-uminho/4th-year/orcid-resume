import { config } from '../../config';
import got from 'got';

const baseUrl = 'https://api.elsevier.com';

export function publicationDetails(doi: string): Promise<JSON> {
  return got(
    `${baseUrl}/content/abstract/doi/${doi}?apiKey=${config.elsevierApiKey}`
  ).json();
}
