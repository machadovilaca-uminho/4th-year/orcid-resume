import { OrcidAddress } from './address';

export interface OrcidOrganization {
  'name': string;
  'address': OrcidAddress;
  'disambiguated-organization': {
    'disambiguated-organization-identifier': string;
  };
}
