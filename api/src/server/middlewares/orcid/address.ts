export interface OrcidAddress {
  'city': string;
  'region': string;
  'country': string;
}
