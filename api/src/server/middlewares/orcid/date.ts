export interface OrcidDate {
  'year': {
    'value': string;
  };
  'month': {
    'value': string;
  };
  'day': {
    'value': string;
  };
}

export function orcidDateToDate(orcidDate: OrcidDate): Date {
  if (orcidDate === null) {
    return null;
  }

  const year = orcidDate.year ? Number(orcidDate.year.value) : null;
  const month = orcidDate.month ? Number(orcidDate.month.value) : null;
  const day = orcidDate.day ? Number(orcidDate.day.value) : null;

  return new Date(year, month, day);
}
