import got from 'got';

const baseUrl = 'https://pub.orcid.org/v2.1/';

export interface OrcidPerson {
  'last-modified-date': {
    'value': string;
  };
  'name': {
    'given-names': {
      'value': string;
    };
    'family-name': {
      'value': string;
    };
  };
  'biography': {
    'content': string;
  };
  'external-identifiers': {
    'external-identifier': [OrcidExternalId];
  };
}

export interface OrcidExternalId {
  'external-id-type': string;
  'external-id-value': string;
}

export function getPerson(orcidId: string): Promise<OrcidPerson> {
  return got(`${baseUrl + orcidId}/person`).json() as Promise<OrcidPerson>;
}
