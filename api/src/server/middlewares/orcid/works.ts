import { OrcidDate } from './date';
import got from 'got';

const baseUrl = 'https://pub.orcid.org/v2.1/';

export interface OrcidWork {
  'put-code': string;
  'last-modified-date': {
    'value': string;
  };
  'source': {
    'source-name': {
      'value': string;
    };
  };
  'title': {
    'title': {
      'value': string;
    };
    'subtitle': string;
    'translated-title': string;
  };
  'external-ids': {
    'external-id': [
      {
        'external-id-type': string;
        'external-id-value': string;
      }
    ];
  };
  'type': string;
  'publication-date': OrcidDate;
}

export interface OrcidWorkGroup {
  'work-summary': [OrcidWork];
}

export interface OrcidWorks {
  'group': [OrcidWorkGroup];
}

export function getWorks(orcidId: string): Promise<OrcidWorks> {
  return got(`${baseUrl + orcidId}/works`).json() as Promise<OrcidWorks>;
}
