import { OrcidDate } from './date';
import { OrcidOrganization } from './organization';
import got from 'got';

const baseUrl = 'https://pub.orcid.org/v2.1/';

export interface OrcidEducation {
  'last-modified-date': {
    'value': string;
  };
  'department-name': string;
  'role-title': string;
  'start-date': OrcidDate;
  'end-date': OrcidDate;
  'organization': OrcidOrganization;
  'put-code': string;
}

export interface OrcidEducations {
  'education-summary': [OrcidEducation];
}

export function getEducations(orcidId: string): Promise<OrcidEducations> {
  return got(`${baseUrl + orcidId}/educations`).json() as Promise<OrcidEducations>;
}
