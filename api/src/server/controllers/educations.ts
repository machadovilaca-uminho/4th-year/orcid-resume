import { Page } from 'objection';
import { createOrGetOrganization } from '../models/organization';
import { getEducations } from '../middlewares/orcid/educations';
import Education, { educationFromOrcid } from '../models/education';
import userController from './users';

function get(userOrcidId: string, page: number): Promise<Page<Education>> {
  return Education.query()
    .join('users', 'userId', '=', 'users.id')
    .join('organizations', 'organizationId', '=', 'organizations.id')
    .select('organizations.name as organizationName', 'departmentName', 'roleTitle', 'startDate', 'endDate')
    .where('users.orcidId', '=', userOrcidId)
    .orderBy('startDate', 'DESC')
    .page(page, 10);
}

async function create(userOrcidId: string): Promise<JSON> {
  let educations = 0;
  const orcidEducations = await getEducations(userOrcidId);

  for (const orcidEducation of orcidEducations['education-summary']) {
    const education = educationFromOrcid(userOrcidId, orcidEducation);

    const count = await Education.query()
      .select('orcidId')
      .where('orcidId', '=', education.orcidId)
      .resultSize();

    if (count === 0) {
      education.$setRelated('user', await userController.get(userOrcidId));
      education.$setRelated('organization', await createOrGetOrganization(orcidEducation.organization));
      await Education.query().insertGraph(education, { relate: true });
      educations++;
    }
  }

  return new Promise(resolve => {
    resolve(JSON.parse(`{ "results": ${educations} }`));
  });
}

export default {
  create,
  get
};
