import { Page } from 'objection';
import { getCitations } from '../middlewares/scopus/citations';
import { getWorks } from '../middlewares/orcid/works';
import Work, { workFromOrcid } from '../models/work';
import userController from './users';
import { publicationDetails } from '../middlewares/scopus/publicationDetails';

async function get(userOrcidId: string, page: number): Promise<Page<Work>> {
  const works = await Work.query()
    .join('users', 'userId', '=', 'users.id')
    .select('works.id', 'title', 'subtitle', 'type', 'date', 'eid', 'doi', 'citations')
    .where('users.orcidId', '=', userOrcidId)
    .orderBy('date', 'DESC')
    .page(page, 10);

  for (const raw of works.results) {
    const work: Work = Object.assign(Work, raw);

    if (work.doi !== null && work.citations === null) {
      work.citations = (await getCitations(work.doi))['search-results'].entry[0]['citedby-count'];
      await Work.query().findById(work.id).patch({ citations: work.citations });
    }
  }

  return works;
}

async function show(userOrcidId: string, id: string): Promise<JSON> {
  const work = await Work.query().findById(id);

  if (work.doi === null) {
    return JSON.parse('{"error": true}');
  }

  return publicationDetails(work.doi);
}

async function create(userOrcidId: string): Promise<JSON> {
  let works = 0;
  const orcidWorks = await getWorks(userOrcidId);

  const user = await userController.get(userOrcidId);

  for (const orcidWorkGroup of orcidWorks.group) {
    for (const orcidWork of orcidWorkGroup['work-summary']) {
      const work = workFromOrcid(userOrcidId, orcidWork);

      const count = await Work.query()
        .select('orcidId')
        .where('orcidId', '=', work.orcidId)
        .resultSize();

      if (count === 0) {
        work.$setRelated('user', user);
        await Work.query().insertGraph(work, { relate: true });
        works++;
      }
    }
  }

  return new Promise(resolve => {
    resolve(JSON.parse(`{ "results": ${works} }`));
  });
}

export default {
  create,
  get,
  show
};
