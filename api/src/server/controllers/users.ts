import { getPerson } from '../middlewares/orcid/person';
import User, { createOrUpdateUser, userFromOrcid } from '../models/user';

async function create(orcidId: string): Promise<User> {
  const orcidPerson = await getPerson(orcidId);
  let user = userFromOrcid(orcidId, orcidPerson);

  user = await createOrUpdateUser(user);

  return new Promise(resolve => {
    resolve(user);
  });
}

async function get(orcidId: string): Promise<User> {
  const user = await User.query()
    .select('id', 'orcidId', 'givenNames', 'familyName', 'biography', 'scopusAuthorId')
    .where('orcidId', '=', orcidId)
    .first();

  if (!user) {
    return create(orcidId);
  }

  return user;
}

export default {
  create,
  get
};
