import * as knex from 'knex';
import { config } from './config';

export const knexConfig: { [index: string]: object } = {};
knexConfig[config.environment] = config.knex;

const knexInstance = knex(knexConfig[config.environment]);

export default knexInstance;
