import * as Knex from 'knex';

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable('educations', table => {
    table.uuid('id').primary().defaultTo(knex.raw('uuid_generate_v4()'));

    table.string('orcidId', 20).unique();
    table.date('lastUpdateDate');
    table.string('departmentName');
    table.string('roleTitle');
    table.date('startDate');
    table.date('endDate');
  });

  await knex.schema.alterTable('educations', table => {
    table.uuid('userId').references('users.id');
    table.uuid('organizationId').references('organizations.id');
  });
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable('educations');
}
