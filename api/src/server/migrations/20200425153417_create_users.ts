import * as Knex from 'knex';

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable('users', table => {
    table.uuid('id').primary().defaultTo(knex.raw('uuid_generate_v4()'));

    table.string('orcidId').unique();
    table.string('scopusAuthorId');
    table.date('lastUpdateDate');
    table.string('givenNames');
    table.string('familyName');
    table.text('biography');
  });
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable('users');
}
