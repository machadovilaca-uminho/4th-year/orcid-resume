import * as Knex from 'knex';

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable('organizations', table => {
    table.uuid('id').primary().defaultTo(knex.raw('uuid_generate_v4()'));

    table.string('orcidId', 20).unique();
    table.string('name');
  });

  await knex.schema.alterTable('organizations', table => {
    table.uuid('addressId').references('addresses.id');
  });
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable('organizations');
}
