import * as Knex from 'knex';

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable('addresses', table => {
    table.uuid('id').primary().defaultTo(knex.raw('uuid_generate_v4()'));

    table.string('country');
    table.string('region');
    table.string('city');
  });
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable('addresses');
}
