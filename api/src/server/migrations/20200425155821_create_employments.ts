import * as Knex from 'knex';

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable('employments', table => {
    table.uuid('id').primary().defaultTo(knex.raw('uuid_generate_v4()'));

    table.string('orcidId', 20).unique();
    table.date('lastUpdateDate');
    table.string('departmentName');
    table.string('roleTitle');
    table.date('startDate');
    table.date('endDate');
    table.string('url');
  });

  await knex.schema.alterTable('employments', table => {
    table.uuid('userId').references('users.id');
    table.uuid('organizationId').references('organizations.id');
  });
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable('employments');
}
