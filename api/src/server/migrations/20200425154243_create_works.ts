import * as Knex from 'knex';

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable('works', table => {
    table.uuid('id').primary().defaultTo(knex.raw('uuid_generate_v4()'));

    table.string('orcidId', 20).unique();
    table.date('lastUpdateDate');
    table.string('title');
    table.string('subtitle');
    table.string('type');
    table.date('date');
    table.string('eid');
    table.string('doi');
    table.string('sourceName');
    table.string('citations');
  });

  await knex.schema.alterTable('works', table => {
    table.uuid('userId').references('users.id');
  });
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable('works');
}
