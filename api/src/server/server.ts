import * as Koa from 'koa';
import * as bodyParser from 'koa-bodyparser';
import { Model } from 'objection';
import { config } from './config';
import { logger } from './logging';
import knexInstance from './knex';
import router from './routes';

Model.knex(knexInstance);

const app = new Koa();

app.use(logger);
app.use(bodyParser());
app.use(router.routes());

app.listen(config.port);

console.log(`Server running on port ${config.port}`);
